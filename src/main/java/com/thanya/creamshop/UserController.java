/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.creamshop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thanyarak Namwong
 */
public class UserController {

    private static ArrayList<Product> productList = new ArrayList<>();

    static {

        load();

    }

    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;

    }

    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;

    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;

    }
//    public static boolean claerAll(int index) {
//        productList.removeAll(productList);
//        save();
//        return true;
//
//    }

    public static ArrayList<Product> getProducts() {

        return productList;

    }

    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;

    }

    public static Product getProduct(int index) {
        return productList.get(index);
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("product.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("product.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
