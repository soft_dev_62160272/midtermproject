/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.creamshop;

/**
 *
 * @author Thanyarak Namwong
 */
public class Product {
    private String id;
    private String name;
    private String brand;
    private String price;
    private String amount;

    public Product(String id, String name, String brand, String price, String amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }
    

    public void setAmount(String amount) {
        this.amount = amount;
    }

    
    public String toString() {
        return "id = " + id + ", name = " + name + ", brand = " + brand + ", price = " + price + ", amount = " + amount ;
    }
    
    
    
    
}
